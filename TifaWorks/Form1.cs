﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Text;
using System.IO;

namespace TifaWorks
{
    public partial class Form1 : Form
    {
        Dictionary<string, FontFamily> fonts = new Dictionary<string, FontFamily>();
        public Form1()
        {
            InitializeComponent();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            if (richTextBox1.Text.Length != 0)
            {
                wordCountLabel.Text = String.Format("{0} Words", richTextBox1.Text.Trim().Split(' ').Length);
            }
            else {
                wordCountLabel.Text = "No Words";
            }
        }

        private void cutToolStripButton_Click(object sender, EventArgs e)
        {
            richTextBox1.Cut();
        }

        private void copyToolStripButton_Click(object sender, EventArgs e)
        {
            richTextBox1.Copy();
        }

        private void pasteToolStripButton_Click(object sender, EventArgs e)
        {
            richTextBox1.Paste();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            UpdateButtons();
            using (InstalledFontCollection col = new InstalledFontCollection()) 
            {
                foreach (FontFamily fa in col.Families) {
                    fontComboBox.Items.Add(fa.Name);
                    fonts.Add(fa.Name, fa);
                }
            }
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Undo();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Redo();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Cut();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Copy();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Paste();
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectAll();
        }

        
        Font ModifyFont(Font f, bool bold, bool italic, bool underline) 
        {
            Font newfont = f;
            if (underline && italic && bold)
                newfont = new Font(newfont, FontStyle.Italic | FontStyle.Underline | FontStyle.Bold);
            else if (bold && italic)
                newfont = new Font(newfont, FontStyle.Italic | FontStyle.Bold);
            else if (bold && underline)
                newfont = new Font(newfont, FontStyle.Underline | FontStyle.Bold);
            else if (underline && italic)
                newfont = new Font(newfont, FontStyle.Italic | FontStyle.Underline);
            else if (bold)
                newfont = new Font(newfont, FontStyle.Bold);
            else if (italic)
                newfont = new Font(newfont, FontStyle.Italic);
            else if (underline)
                newfont = new Font(newfont, FontStyle.Underline);
            else
                newfont = new Font(newfont, FontStyle.Regular);
            return newfont;
        }

        private void richTextBox1_SelectionChanged(object sender, EventArgs e)
        {
            UpdateButtons();
        }
        void UpdateButtons() 
        {
            Font f = richTextBox1.SelectionFont;
            HorizontalAlignment ha = richTextBox1.SelectionAlignment;
            try
            {
                boldButton.Checked = f.Bold;
                italicButton.Checked = f.Italic;
                underlineButton.Checked = f.Underline;
                fontComboBox.Text = f.FontFamily.Name;
                sizeComboBox.Text = f.Size.ToString();
                if (ha == HorizontalAlignment.Left) 
                {
                    leftAtoolStripButton.Checked = true;
                    centerAtoolStripButton.Checked = false;
                    rightAtoolStripButton.Checked = false;
                }
                else if (ha == HorizontalAlignment.Center) 
                {
                    leftAtoolStripButton.Checked = false;
                    centerAtoolStripButton.Checked = true;
                    rightAtoolStripButton.Checked = false;
                }
                else if (ha == HorizontalAlignment.Right) 
                {
                    leftAtoolStripButton.Checked = false;
                    centerAtoolStripButton.Checked = false;
                    rightAtoolStripButton.Checked = true;
                }
            }
            catch {
                boldButton.Checked = false;
                italicButton.Checked = false;
                underlineButton.Checked = false;
                fontComboBox.Text = "";
                sizeComboBox.Text = "";

            }
        }

        private void boldButton_CheckedChanged(object sender, EventArgs e)
        {
            UpdateFont();
        }

        private void italicButton_CheckedChanged(object sender, EventArgs e)
        {
            UpdateFont();
        }

        private void underlineButton_CheckedChanged(object sender, EventArgs e)
        {
            UpdateFont();
        }

        void UpdateFont()
        {
            richTextBox1.SelectionFont = ModifyFont(richTextBox1.SelectionFont, boldButton.Checked,
                italicButton.Checked,
                underlineButton.Checked);
            UpdateButtons();
        }

        private void fontComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (fonts.ContainsKey(fontComboBox.Text)) {
                try
                {
                    richTextBox1.SelectionFont = new Font(fonts[fontComboBox.Text], richTextBox1.SelectionFont.Size);
                }
                catch {
                    MessageBox.Show("Unable to use font");
                }
                UpdateFont();
            }
        }

        private void sizeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            float size = float.Parse(sizeComboBox.Text);
            if (size > 1) {
                richTextBox1.SelectionFont = new Font(richTextBox1.SelectionFont.FontFamily, size);
                UpdateFont();
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
        }

        #region Color
        private void blackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionColor = Color.Black;
        }

        private void maroonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionColor = Color.Maroon;
        }

        private void greenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionColor = Color.Green;
        }

        private void oliveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionColor = Color.Olive;
        }

        private void navyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionColor = Color.Navy;
        }

        private void purpleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionColor = Color.Purple;
        }

        private void tealToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionColor = Color.Teal;
        }

        private void grayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionColor = Color.Gray;
        }

        private void silverToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionColor = Color.Silver;
        }

        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionColor = Color.Red;
        }

        private void limeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionColor = Color.Lime;
        }

        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionColor = Color.Blue;
        }

        private void fuchsiaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionColor = Color.Fuchsia;
        }

        private void aquaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionColor = Color.Aqua;
        }

        private void whiteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionColor = Color.White;
        }
        #endregion

        private void customToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) 
            {
                richTextBox1.SelectionColor = colorDialog1.Color;
            }
        }

        private void toolStripTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (toolStripTextBox1.Text.StartsWith("#"))
                {
                    richTextBox1.SelectionColor = Color.FromArgb(int.Parse(toolStripTextBox1.Text.TrimStart('#'), System.Globalization.NumberStyles.HexNumber));
                }
                else if (Color.FromName(toolStripTextBox1.Text) != null) 
                {
                    richTextBox1.SelectionColor = Color.FromName(toolStripTextBox1.Text);
                }
                e.Handled = true;
                toolStripTextBox1.Clear();
            }
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionBackColor = Color.Black;
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionBackColor = Color.Maroon;
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionBackColor = Color.Green;
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionBackColor = Color.Olive;
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionBackColor = Color.Navy;
        }

        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionBackColor = Color.Purple;
        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionBackColor = Color.Teal;
        }

        private void toolStripMenuItem9_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionBackColor = Color.Gray;
        }

        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionBackColor = Color.Silver;
        }

        private void toolStripMenuItem11_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionBackColor = Color.Red;
        }

        private void toolStripMenuItem12_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionBackColor = Color.Lime;
        }

        private void toolStripMenuItem13_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionBackColor = Color.Yellow;
        }

        private void toolStripMenuItem14_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionBackColor = Color.Blue;
        }

        private void toolStripMenuItem15_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionBackColor = Color.Fuchsia;
        }

        private void toolStripMenuItem16_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionBackColor = Color.Aqua;
        }

        private void toolStripMenuItem17_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionBackColor = Color.White;
        }

        private void noneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionBackColor = Color.Transparent;
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            StreamReader sr = new StreamReader(openFileDialog1.OpenFile());
            richTextBox1.Clear();
            richTextBox1.Rtf = sr.ReadToEnd();
            sr.Close();
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            StreamWriter sw = new StreamWriter(saveFileDialog1.OpenFile());
            sw.Write(richTextBox1.Rtf);
            sw.Close();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionAlignment = HorizontalAlignment.Left;
            UpdateButtons();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionAlignment = HorizontalAlignment.Center;
            UpdateButtons();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionAlignment = HorizontalAlignment.Right;
            UpdateButtons();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ab = new AboutBox1();
            ab.ShowDialog();
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void toolStripTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (toolStripTextBox1.Text.StartsWith("#"))
                {
                    richTextBox1.SelectionBackColor = Color.FromArgb(int.Parse(toolStripTextBox1.Text.TrimStart('#'), System.Globalization.NumberStyles.HexNumber));
                }
                else if (Color.FromName(toolStripTextBox1.Text) != null)
                {
                    richTextBox1.SelectionBackColor = Color.FromName(toolStripTextBox1.Text);
                }
                else {
                    richTextBox1.SelectionBackColor = Color.Transparent;
                }
                e.Handled = true;
                toolStripTextBox1.Clear();
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                richTextBox1.SelectionBackColor = colorDialog1.Color;
            }
        }
    }
}
